#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
download nsfw pic from pawoo's local time line
"""

import argparse
import os
import six

import requests
from mastodon import Mastodon
from mastodon.streaming import StreamListener


class Listener(StreamListener):
    """
    StreamAPI Listener
    """

    def __init__(self):
        self.updates = []
        self.notifications = []
        self.deletes = []
        self.heartbeats = 0

    def on_update(self, status):
        count = status["media_attachments"]
        if len(count) != 0 and status["sensitive"]:
            username = status["account"]["username"]
            for media in count:
                url = media["url"].split("?")[0]
                print(url, username)
                download(url, username)
        self.updates.append(status)

    def on_notification(self, notification):
        self.notifications.append(notification)

    def on_delete(self, status_id):
        self.deletes.append(status_id)

    def handle_heartbeat(self):
        self.heartbeats += 1

    def handle_stream_(self, lines):
        '''Test helper to avoid littering all tests with six.b().'''
        return self.handle_stream(map(six.b, lines))


def download(url, username):
    """
    download the image to directory of the uploaded user name
    """
    os.makedirs(os.path.join("img", username), exist_ok=True)
    path = os.path.join("img", username, url.split("/")[-1])
    res = requests.get(url)
    if res.status_code != 200:
        print("download failed")
        return
    with open(path, "wb") as img_file:
        img_file.write(res.content)


def register(user_id, password):
    """
    Register app and login
    """
    if not os.path.exists('pawoo_clientcred.txt'):
        Mastodon.create_app(
            'com.laalacity.katsuonotataki',
            api_base_url='https://pawoo.net',
            scopes=['read'],
            to_file='pawoo_clientcred.txt'
        )
    mastodon = Mastodon(
        client_id='pawoo_clientcred.txt',
        api_base_url='https://pawoo.net'
    )
    mastodon.log_in(
        user_id,
        password,
        scopes=['read'],
        to_file='pawoo_usercred.txt'
    )


def main():
    """
    connet local time line
    """
    parser = argparse.ArgumentParser(description='Download nsfw picture')
    parser.add_argument('-u', help='user_id')
    parser.add_argument('-p', help='password')
    args = parser.parse_args()
    if args.u and args.p:
        register(args.u, args.p)

    listener = Listener()
    mastodon = Mastodon(
        client_id='pawoo_clientcred.txt',
        api_base_url='https://pawoo.net',
        access_token='pawoo_usercred.txt'
    )
    print('running...')
    mastodon.public_stream(listener)


if __name__ == '__main__':
    main()
