# katsuo-no-tataki

[![](http://img.youtube.com/vi/5bSiaFDnq54/0.jpg)](https://www.youtube.com/watch?v=5bSiaFDnq54)

---

## Installation

```
$ git clone https://bitbucket.org/laalacity/katsuo-no-tataki.git
$ pip install -e git+https://github.com/halcy/Mastodon.py.git#egg=Mastodon.py
```

## Usage
First run
```
$ python katsuonotataki.py -u user_id -p password
```

After registration, ID and password are not required
```
$ python katsuonotataki.py
```
